# Xpand IT Spark Challenge

This project contains the code of my solution to [this challenge](https://github.com/bdu-xpand-it/BDU-Recruitment-Challenges/wiki/Spark-2-Recruitment-Challenge). 
This challenge consists in developing a Spark application (in Scala) that processes
2 `.csv` files and outputs the results of some transformations to `.parquet` files.

## To Do List

- [x] Develop the application in Scala to solve the challenge.
- [ ] Finalize tests and do small refactoring to make it cleaner.
- [ ] Terminate the documentation of this **README.md**.
- [ ] Add a **Dockerfile** for containerizing this application.
- [ ] Setup the CI pipeline (build image and push to DockerHub) using GitLab CI.


## How to run

After having cloned the repository and buit the project using Maven, one can 
run the application using the following command:

```bash
$ spark-submit --class com.gitlab.psoliveira.App \
--master local[8] \ 
target/xpand-it-spark-1.0-SNAPSHOT.jar
```

This assumes you have Spark installed on your machine and the `/bin` folder properly
configured in your **$PATH**.