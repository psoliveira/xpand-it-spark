package com.gitlab.psoliveira

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._


/**
 * Xpand IT Spark 2 Challenge: develop a Spark 2 application (in Scala) that processes 2 CSV
 * files (google-play-store-apps.zip).
 *
 * @author Pedro Oliveira
 */
object App {

  
  def main(args : Array[String]) {
    val spark = SparkSession
      .builder
      .appName("Xpand IT Spark Challenge")
      .getOrCreate()

    val df = spark.read
      .option("inferSchema", "true")
      .option("header", "true")
      .csv("google-play-store-apps/googleplaystore_user_reviews.csv")

    // import required for being able to work with the $-notation for selecting a column
    import spark.implicits._

    // Part 1 - avg app sentiment polarity (from user reviews)
    val df_1 = df.select(col("App"), col("Sentiment_Polarity").cast("double"))
      .na.fill(0)
      .groupBy("App")
      .agg(mean($"Sentiment_Polarity") as "Average_Sentiment_Polarity")

    // Part 2 - best apps (highest rating)
    val df_apps = spark.read
      .option("inferSchema", "true")
      .option("header", "true")
      .csv("google-play-store-apps/googleplaystore.csv")

    //  coalesce(1) is used to keep a single .csv file (although inside the .csv/ folder)
    df_apps.filter($"Rating" >= 4)
      .sort($"Rating".desc)
      .coalesce(1)
      .write
      .option("sep", "§")
      .option("header", "true")
      .mode("overwrite")
      .csv("best_apps.csv")

    // Part 3 - app summary info from entry with the max number of reviews

    // a. obtain the apps' categories as list (no app duplicates here)
    val df_cat = df_apps.groupBy($"App")
      .agg(collect_set($"Category") as "Categories")

    // b.
    val df_apps_cleaned = df_apps.withColumnRenamed("Content Rating", "Content_Rating")
      .withColumnRenamed("Last Updated", "Last_Updated")
      .withColumnRenamed("Current Ver", "Current_Version")
      .withColumnRenamed("Android Ver", "Minimum_Android_Version")
      .withColumn("Last_Updated", to_date(col("Last_Updated"), "MMM dd, yyyy"))
      .withColumn("Genres", split(col("Genres"), ";").cast("array<string>"))
      .withColumn("Reviews", col("Reviews").cast("long"))
      .withColumn("Size", regexp_extract(col("Size"), "(.*).$", 1).cast("double")
      * when(col("Size").endsWith("M"), 1000000).otherwise(1000))
      .withColumn("has_dollar", col("Price").startsWith("$"))
      .withColumn("Price",
        ltrim($"Price", "$").cast("double") * when(col("has_dollar"), 0.9).otherwise(1))
      .withColumn("Price", rpad($"Price", 1, "€"))
      .drop("has_dollar", "Category")
      .filter($"Reviews".isNotNull)


    // c. obtain the entry with the max number of reviews
    val df_max_reviews = df_apps_cleaned.groupBy($"App")
      .agg(max($"Reviews") as "Reviews", max("Last_Updated") as "Last_Updated")

    // d. merge apps DF with max reviews DF
    val df_merge = df_apps_cleaned.join(df_max_reviews, Seq("App", "Reviews", "Last_Updated"))
      .dropDuplicates()

    val df_2 = df_merge.join(df_cat, "App")

    // Part 4 - add avg sentiment polarity to apps summary table (df_2)
    val df_3 = df_2.join(df_1, Seq("App"), "left")
    df_3.write
      .option("compression", "gzip")
      .mode("overwrite")
      .parquet("googleplaystore_cleaned.parquet")

    val df_4 = df_3.withColumn("Genre", explode($"Genres"))
      .groupBy($"Genres")
      .agg(count($"App") as "Count",
        mean($"Rating") as "Average_Rating",
        mean("Average_Sentiment_Polarity") as "Average_Sentiment_Polarity")

    df_4.write
      .option("compression", "gzip")
      .mode("overwrite")
      .parquet("googleplaystore_metrics.parquet")
    spark.stop()
  }
}
